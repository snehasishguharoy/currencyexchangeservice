package com.example.microservices.currencyexchangeservice.currencyexchangeservice.svc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservices.currencyexchangeservice.currencyexchangeservice.entity.ExchangeValue;
import com.example.microservices.currencyexchangeservice.currencyexchangeservice.repository.ExchangeValueRepository;

@RestController
public class CurrencyExchangeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyExchangeController.class);
	@Autowired
	private Environment environment;
	@Autowired
	private ExchangeValueRepository exchangeValueRepository;

	@PostMapping("/from/{from}/to/{to}")
	public ResponseEntity<ExchangeValue> retrieveExchangeValue(
			@PathVariable("from") String from, @PathVariable("to") String to) {
		LOGGER.info("Currency Exchange");
		ExchangeValue exchangeValue = exchangeValueRepository.findByFromAndTo(from, to);
		exchangeValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
		return new ResponseEntity<ExchangeValue>(exchangeValue, HttpStatus.OK);
	//	return exchangeValue;
	}

}
